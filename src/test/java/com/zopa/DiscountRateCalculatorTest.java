package com.zopa;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DiscountRateCalculatorTest {

    /**
     * Calculate the equivalent annual interest rate given a loan amount and repayment amount.
     *
     * (Example figures from: http://budgeting.thenest.com/calculate-interest-rate-using-present-future-value-25993.html
     * and changed period from 10 to 3 yrs)
     */
    @Test
    public void calculate() {
        assertEquals(0.205, new DiscountRateCalculator().calculate(1750, 1000), 0.001);
    }
}
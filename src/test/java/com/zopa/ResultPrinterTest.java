package com.zopa;

import com.zopa.FinalRepaymentAmountCalculator.RateAndAmount;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class ResultPrinterTest {

    @Test
    public void printToPrintStream() throws UnsupportedEncodingException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(baos);

        new ResultPrinter().printResultsToConsole(stream, 1000, new RateAndAmount(0.07, 1108.10));

        assertThat(baos.toString("UTF8"), is("Requested amount: £1000\nRate: 7.0%\nMonthly repayment: £30.78\nTotal repayment: £1108.10\n"));
    }
}
package com.zopa;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SingleLenderRepaymentAmountCalculatorTest {

    /**
     * Loan amount 480, rate 6.9%, compounding monthly over 36months, repayment amount should be:
     * repayment = principal * (1 + rate/compounding periods per year)^(compounding periods * years)
     * repayment = 480 *(1 + 0.069/12)^36 = 590.04
     */
    @Test
    public void calculate() {
        double repayment = new SingleLenderRepaymentAmountCalculator().calculateRepayment(480, 0.069);
        assertEquals(590.04, repayment, 0.01); // correct to nearest penny
    }
}

package com.zopa;

import com.zopa.FinalRepaymentAmountCalculator.RateAndAmount;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class FinalRepaymentAmountCalculatorTest {

    /**
     * Test scenario:
     *
     * Loan amount 1000 => borrowed from two lowest rate lenders (520 + 480)
     * Amount repayable = 590.04 + 643.04 = 1233.08
     * Annual rate = (1233.08/1000)^(1/3) - 1 = 7.2%
     */
    @Test
    public void calculate() {
        File file = new File(getClass().getClassLoader().getResource("rates.csv").getFile());

        RateAndAmount rateAndAmount = new FinalRepaymentAmountCalculator(file).calculate(1000);
        assertEquals(rateAndAmount.getAmount(), 1233.08, 0.01);
        assertEquals(rateAndAmount.getRate(), 0.072, 0.01);
    }
}
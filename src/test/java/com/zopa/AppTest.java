package com.zopa;

import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.hamcrest.CoreMatchers;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AppTest {

    private static File tempDir;

    @BeforeClass
    public static void setUpTempFile() {
        tempDir = Files.createTempDir();
    }

    @Test
    public void validateNonExistentFile() {
        File ratesFile = new File(tempDir, "rates.csv");
        try {
            App.main(new String[]{ratesFile.getAbsolutePath(), "1500"});
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Market file [" + ratesFile.getAbsolutePath() + "] doesn't exist"));
        }
    }

    @Test
    public void validateLowAmount() throws IOException {
        File ratesFile = new File(tempDir, "rates.csv");
        ratesFile.createNewFile();
        try {
            App.main(new String[]{ratesFile.getAbsolutePath(), "500"});
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Amount [500] is not between £1000 and £15000 and a whole number of £100s"));
        }
    }

    @Test
    public void validateNon100Amount() throws IOException {
        File ratesFile = new File(tempDir, "rates.csv");
        ratesFile.createNewFile();
        try {
            App.main(new String[]{ratesFile.getAbsolutePath(), "1750"});
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Amount [1750] is not between £1000 and £15000 and a whole number of £100s"));
        }
    }


    @Test
    public void validateNonNumericmount() throws IOException {
        File ratesFile = new File(tempDir, "rates.csv");
        ratesFile.createNewFile();
        try {
            App.main(new String[]{ratesFile.getAbsolutePath(), "1750abc"});
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Amount [1750abc] is not numeric. Amounts must be integers (and not include decimal points)"));
        }
    }

    @Test
    public void validateInsufficientBorrowingPool() throws IOException {
        File ratesFile = new File(getClass().getClassLoader().getResource("rates.csv").getFile());
        try {
            App.main(new String[]{ratesFile.getAbsolutePath(), "5000"});
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Borrowing pool is too small (£2330) to offer loan of £5000"));
        }
    }

    @Test
    public void calculateRateAndRepaymentAmount() throws IOException {
        File ratesFile = new File(getClass().getClassLoader().getResource("rates.csv").getFile());
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
        App.main(new String[]{ratesFile.getAbsolutePath(), "1000"});

        String expectedConsoleOutput = "Requested amount: £1000\nRate: 7.2%\nMonthly repayment: £34.25\nTotal repayment: £1233.08\n";
        assertThat(output.toString("UTF8"), is(expectedConsoleOutput));
    }


    @AfterClass
    public static void tidy() throws IOException {
        FileUtils.deleteDirectory(tempDir);
    }
}

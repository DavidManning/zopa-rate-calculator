package com.zopa;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class RateFileReaderTest {

    @Test
    public void loadRatesFromCSV() throws IOException {
        File file = new File(getClass().getClassLoader().getResource("rates.csv").getFile());

        RateFileReader calculator = new RateFileReader(file);
        List<RateFileReader.SingleRateAndAmount> records = calculator.readRatesFromFile();

        assertThat(records.size(), is(7));
        assertThat(records.get(0).getAvailable(), is(480));
        assertThat(records.get(0).getRate(), is(0.069));
    }
}
package com.zopa;

import java.io.File;
import java.util.List;


public class FinalRepaymentAmountCalculator {

    private final File ratesFile;

    public FinalRepaymentAmountCalculator(File ratesFile) {
        this.ratesFile = ratesFile;
    }

    /**
     * Calculates the best rate and final repayable amount given a valid market rates files
     * and borrowing amount.
     *
     * @param amount The borrowing amount
     * @return The best rate and amount
     */
    public RateAndAmount calculate(int amount) {
        List<RateFileReader.SingleRateAndAmount> rates = new RateFileReader(ratesFile).readRatesFromFile();
        int amountOutstanding = amount;
        double amountRepayable = 0d;

        for (RateFileReader.SingleRateAndAmount rate : rates) {
            // Take everything from the lender, or the amount outstanding if, whatever's less
            int amountBorrowedFromLender = Math.min(rate.getAvailable(), amountOutstanding);
            amountOutstanding -= amountBorrowedFromLender;

            amountRepayable += new SingleLenderRepaymentAmountCalculator().calculateRepayment(amountBorrowedFromLender, rate.getRate());

            // only continue to accept from lender until the required borrowing amount has been obtained
            if (amountOutstanding == 0) {
                break;
            }
        }

        double annualRate = new DiscountRateCalculator().calculate(amountRepayable, amount);
        return new RateAndAmount(annualRate, amountRepayable);
    }

    /**
     * The result of a repayment rate and amount calculation.
     */
    public static final class RateAndAmount {

        private final double rate;

        private final double amount;

        public RateAndAmount(double rate, double amount) {
            this.rate = rate;
            this.amount = amount;
        }

        public double getRate() {
            return rate;
        }

        public double getAmount() {
            return amount;
        }
    }
}

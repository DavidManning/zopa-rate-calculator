package com.zopa;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Calculates repayment amounts for a single lender.
 */
public class SingleLenderRepaymentAmountCalculator {

    /**
     * Calculates the repayment amount due after 36 months (assume calendar months - 36mths = 3yrs)
     * for a loan for the specified value
     *
     * @param loanAmount The amount being lent (assume it's an integer value as given in the example input file)
     * @param rate The annual interest rate
     * @return The repayment amount
     */
    public double calculateRepayment(int loanAmount, double rate) {
        return loanAmount * Math.pow((rate/12) + 1, 36);
    }
}

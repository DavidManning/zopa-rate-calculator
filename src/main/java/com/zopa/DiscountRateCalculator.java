package com.zopa;

public class DiscountRateCalculator {

    /**
     * Based on the borrowing amount (present value) and the future value (final
     * repayment amount), calculates the 'blended' annual interest rate.
     *
     * @param futureValue The future value (full repayable amount)
     * @param presentValue The present value (loan amount)
     * @return The interest rate
     */
    public double calculate(double futureValue, double presentValue) {
        return (Math.pow((futureValue/presentValue), (1.0/3.0)) - 1);
    }
}

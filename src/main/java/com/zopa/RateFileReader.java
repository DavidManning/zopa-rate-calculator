package com.zopa;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import static java.lang.Integer.parseInt;
import static java.util.Collections.sort;
import static java.util.Comparator.comparingDouble;
import static java.util.stream.Collectors.toList;

/**
 * Extracts useful information from a rates files in CSV format.
 */
public class RateFileReader {

    private final File ratesFile;

    public RateFileReader(File ratesFile) {
        this.ratesFile = ratesFile;
    }

    /**
     * Reads rates from the input CSV and sorts them from lowest to highest rate.
     */
    public List<SingleRateAndAmount> readRatesFromFile() {

        List<CSVRecord> records = getLenderRatesAndAmounts();

        List<SingleRateAndAmount> rates = records.stream().map(record -> new SingleRateAndAmount(record.get("Available"), record.get("Rate"))).collect(toList());
        sort(rates, comparingDouble(SingleRateAndAmount::getRate));
        return rates;
    }

    public int getTotalAvailableFromBorrowingPool() {
        return getLenderRatesAndAmounts().stream().map(rate -> parseInt(rate.get("Available"))).reduce(0, (x, y) -> x + y);
    }

    private List<CSVRecord> getLenderRatesAndAmounts() {
        List<CSVRecord> records = null;
        try {
            return records = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(new FileReader(ratesFile)).getRecords();
        } catch (IOException e) {
            throw new IllegalArgumentException("Unable to read rates file");
        }
    }

    public static final class SingleRateAndAmount {
        private final int available;

        private final double rate;

        public SingleRateAndAmount(String available, String rate) {
            this.available = parseInt(available);
            this.rate = Double.parseDouble(rate);
        }

        public int getAvailable() {
            return available;
        }

        public double getRate() {
            return rate;
        }

    }
}

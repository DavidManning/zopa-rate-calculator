package com.zopa;

import com.zopa.FinalRepaymentAmountCalculator.RateAndAmount;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.Integer.parseInt;
import static java.lang.String.format;

/**
 * Rate Calculator Cli. This is effectively the controller for the application.
 */
public class App {

    private static String ratesFilePath;

    private static int amount;

    public static void main(String[] args) {
        validateArgs(args);

        RateAndAmount rateAndAmount = new FinalRepaymentAmountCalculator(new File(ratesFilePath)).calculate(amount);

        new ResultPrinter().printResultsToConsole(System.out, amount, rateAndAmount);
    }

    private static void validateArgs(String[] args) {
        // Validate filename
        checkArgument(args.length == 2, "Incorrect number of arguments provided");
        checkArgument(new File(args[0]).exists(), format("Market file [%s] doesn't exist", args[0]));
        ratesFilePath = args[0];

        // Validate amount
        checkArgument(StringUtils.isNumeric(args[1]), format("Amount [%s] is not numeric. Amounts must be integers (and not include decimal points)", args[1]));
        amount = parseInt(args[1]);
        checkArgument(amount % 100 == 0 && amount >= 1000 && amount <= 15000,
                format("Amount [%s] is not between £1000 and £15000 and a whole number of £100s", args[1]));

        // Validate amount requested is available in pool
        int totalAvailableFromBorrowingPool = new RateFileReader(new File(ratesFilePath)).getTotalAvailableFromBorrowingPool();
        checkArgument(totalAvailableFromBorrowingPool >= amount, format("Borrowing pool is too small (£%d) to offer loan of £%d", totalAvailableFromBorrowingPool, amount));
    }
}

package com.zopa;

import com.zopa.FinalRepaymentAmountCalculator.RateAndAmount;

import java.io.PrintStream;

public class ResultPrinter {

    /**
     * Prints the results of the calculation to the designated PrintStream. Example Format:
     * Requested amount: £1000 \nRate: 7.0%\nMonthly repayment: £30.78\nTotal repayment: £1108.10
     */
    public void printResultsToConsole(PrintStream output, int requestedAmount, RateAndAmount result) {
        output.println("Requested amount: £" + requestedAmount);
        output.printf("Rate: %.1f%%", result.getRate() * 100).println();
        // TODO figure out what to do with rounding - the monthly repayment amount could be out by up to 36p!
        output.printf("Monthly repayment: £%.2f", result.getAmount()/36).println();
        output.printf("Total repayment: £%.2f", result.getAmount()).println();
    }
}

## Rate Calculator Cli Application

This command line java application calculated repayment rates and amounts for loan amounts given a market rates input CSV.

## Building
Build with maven using: mvn package

This will produce a runnable ‘fat’ .jar named ratecalculator-x.x-SNAPSHOT-jar-with-dependencies.jar

## Running
The runnable .jar can be run from the command line:

java -jar ratecalculator-1.0-SNAPSHOT-jar-with-dependencies.jar [rates_file], [amount]



